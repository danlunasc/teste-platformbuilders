package com.platformbuilders.teste.Cliente;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    public Page<Cliente> findByNomeIgnoreCaseContaining(String nome, Pageable page);
    public Page<Cliente> findByCpfIgnoreCaseContaining(String cpf, Pageable page);
}

package com.platformbuilders.teste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestePlatformBuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestePlatformBuildersApplication.class, args);
	}

}
